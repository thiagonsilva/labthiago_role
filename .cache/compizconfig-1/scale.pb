
y���pt_BR���  ( 2scale:DimensionarBDimensionar janelasJWindow ManagementZ%
fade
decor
imgpngopenglimgpng�
;
spacing"Espaçamento*Espaço entre janelas0 Z(` h�
>
x_offset"X Offset*Horizontal offset (in pixels).0 Z ` 
I
y_offset"Y Offset*)Vertical offset from the top (in pixels).0 Z ` 
Z
y_bottom_offset"Y Bottom Offset*,Vertical offset from the bottom (in pixels).0 Z ` 
N
speed"
Velocidade*Velocidade de Dimensionamento0 Z  �@}���=�  HB����=
T
timestep"Intervalo de tempo*Etapa de dimensionamento0 Z���=}���=�  HB����=
^
darken_back "Escurecer Segundo Plano*.Escurecer segundo plano ao dimensionar janelas0 Z
F
opacity"	Opacidade*"Volume de opacidade em percentagem0 Z�` h�
�
overlay_icon"Ícone de sobreposição*=Sobrepor um ícone nas janelas quando elas são dimensionadas0 Z ` hr
 NenhumrEmblemar
Grande
�
window_match
"Janelas a dimensionar*1Janelas a serem dimensionadas no modo Dimensionar0Z/2-Toolbar | Utility | Dialog | Normal | Unknown
�

hover_time"Drag and Drop Hover Timeout*fTime (in ms) before scale mode is terminated when hovering over a window dragging and dropping an item0Z�`dh��
t
dnd_distance"Drag and Drop Distance*?The minimum distance (in px) beyond which the timeout is reset.0Z` h�
�
dnd_timeout_spinner ".Show a spinner during th Drag and Drop timeout*>Whether to show a spinner over the windows during dnd Timeout.0Z
�
dnd_timeout_spinner_speed"$The speed of the DnD timeout spinner*9The animation speed (in ms) of the Drag and Drop spinner.0Z�`dh�
�
multioutput_mode"Modo de Multi Saída*\Seleciona onde as janelas são dimensionadas se existirem múltiplos dispositivos de saída.0Z` hr) %Apenas no dispositivo de saída atualr&"Em todos os dispositivos de saída
I
skip_animation "Skip Animation*!Skips the scale plugin animation.0Z 
�
key_bindings_toggle "-Tecla de Atalho Alternar Modo Dimensionamento*wTecla de atalho que alterna o modo de dimensionamento em vez de o activar quando premida e desactivar quando libertada.0Z
�
button_bindings_toggle ".Botão de Atalho Alternar Modo Dimensionamento*yBotão de atalho que alterna o modo de dimensionamento em vez de o activar quando premido ou desactivar quando libertado.0Z 
g
initiate_edge"Iniciar Seletor de Janelas*6Criar layout das janelas e iniciar sua transformação0
r
initiate_key"Iniciar Seletor de Janelas*6Criar layout das janelas e iniciar sua transformação0Z
2<Super>w
i
initiate_button"Iniciar Seletor de Janelas*6Criar layout das janelas e iniciar sua transformação0
�
initiate_all_edge"1Iniciar Selector de Janelas para Todas as Janelas*>Criar layout de todas as janelas e iniciar sua transformação0
�
initiate_all_button"1Iniciar Selector de Janelas para Todas as Janelas*>Criar layout de todas as janelas e iniciar sua transformação0
�
initiate_all_key"1Iniciar Selector de Janelas para Todas as Janelas*>Criar layout de todas as janelas e iniciar sua transformação0Z2<Super><Shift>w
�
initiate_group_edge"1Iniciar Selector de Janelas para Grupo de Janelas*>Criar layout de grupo de janelas e iniciar sua transformação0
�
initiate_group_button"1Iniciar Selector de Janelas para Grupo de Janelas*>Criar layout de grupo de janelas e iniciar sua transformação0
�
initiate_group_key"1Iniciar Selector de Janelas para Grupo de Janelas*>Criar layout de grupo de janelas e iniciar sua transformação0
�
initiate_output_edge"AIniciar Selector de Janelas para Todas as Janelas na Saída Atual*ECriar layout de janelas na saída atual e iniciar sua transformação0
�
initiate_output_button"AIniciar Selector de Janelas para Todas as Janelas na Saída Atual*ECriar layout de janelas na saída atual e iniciar sua transformação0
�
initiate_output_key"AIniciar Selector de Janelas para Todas as Janelas na Saída Atual*ECriar layout de janelas na saída atual e iniciar sua transformação0
�
click_on_desktop"Click on empty area*(Action to perform on click on empty area0Z` hr
 NenhumrShow desktoprTerminate scale
AparênciaComportamentoAtalhos